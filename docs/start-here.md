#Diagrams created with this link: https://foambubble.github.io/foam/user/recipes/diagrams-in-markdown.html


Overall diagram

![Here is the POC context](image.png)

Express Gateway documentation Link:

https://developerengine.fisglobal.com/apis/express/express-xml/responses

Actual documentation link: 
https://payrix.atlassian.net/wiki/spaces/PE/pages/23688675496/Express+Gateway+Integration+REVIEW

Current scope:

- Request from postman -> Core APi -> Integration -> XML/SOAP => Express Gateway API


Implementation MR CODE:

https://gitlab.com/payrix/api/api/-/merge_requests/3984/diffs#5b7c8afeb62e885886bf7ed21968af72748ec414


Postman request used:

https://payrx1.postman.co/workspace/Core-API-Engineering~89ae8f21-3729-4c14-a963-c65356299b53/request/19300626-96fd71d0-696c-43e9-9b8f-32c12efdcf70


Metrics:

- to be evaluated when it is deployed into qa environment as they might be different from dev machine

Next steps:
- merge code to develop
- convert using the transform/translator to use dynamic data from the transaction - documentation needed
- Convert to sending a new object to the microservice express gateway microservice
    - including it in the docker compose
    - orchestration to use this
    - connect to database

