#!/bin/bash

# Function to check if a command exists
command_exists() {
    command -v "$1" >/dev/null 2>&1
}

# Function to install Homebrew if not installed
install_homebrew() {
    if ! command_exists brew; then
        echo "Homebrew not found. Installing Homebrew..."
        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    else
        echo "Homebrew is already installed."
    fi
}

# Function to install .NET SDK if not installed
install_dotnet() {
    if ! command_exists dotnet; then
        echo ".NET SDK not found. Installing .NET SDK..."
        brew install --cask dotnet-sdk
    else
        echo ".NET SDK is already installed."
    fi
}

# Function to install VS Code if not installed
install_vscode() {
    if ! command_exists code; then
        echo "VS Code not found. Installing VS Code..."
        brew install --cask visual-studio-code
    else
        echo "VS Code is already installed."
    fi
}

# Function to install Node.js if not installed
install_node() {
    if ! command_exists node; then
        echo "Node.js not found. Installing Node.js..."
        brew install node
    else
        echo "Node.js is already installed."
    fi
}

# Function to install extensions for VS Code
install_vscode_extensions() {
    echo "Installing VS Code extensions..."
    code --install-extension ms-dotnettools.csharp
    code --install-extension ms-dotnettools.vscode-dotnet-runtime
    code --install-extension ms-vscode.cpptools
    code --install-extension ms-azuretools.vscode-docker
    code --install-extension eamodio.gitlens
    code --install-extension ms-vscode-remote.remote-containers
    code --install-extension ms-vscode-remote.remote-ssh
    code --install-extension ms-vscode-remote.remote-ssh-edit
    code --install-extension ms-vscode-remote.remote-wsl
    code --install-extension ms-vscode.powershell
}

# Function to install Xcode Command Line Tools if not installed
install_xcode_cli() {
    if ! xcode-select -p >/dev/null 2>&1; then
        echo "Xcode Command Line Tools not found. Installing Xcode Command Line Tools..."
        xcode-select --install
    else
        echo "Xcode Command Line Tools are already installed."
    fi
}

# Execute functions
install_homebrew
install_dotnet
install_vscode
install_node
install_vscode_extensions
install_xcode_cli

echo "Environment setup is complete. You can now run, build, and debug .NET Core applications using VS Code."
