dotnet tool install -g AWS.CodeArtifact.NuGet.CredentialProvider
dotnet codeartifact-creds install
dotnet nuget add source --name payrix/Payrix.NET https://payrix-812539651325.d.codeartifact.us-east-1.amazonaws.com/nuget/Payrix.NET/v3/index.json

aws codeartifact login --tool dotnet --domain payrix --repository Payrix.NET --region us-east-1