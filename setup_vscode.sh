#!/bin/bash

# Navigate to your project directory (adjust this path as necessary)
cd "$HOME/git/payrix.auth"

# Create the .vscode directory if it doesn't exist
mkdir -p .vscode

# Navigate into the .vscode directory
cd .vscode

# Create or overwrite the launch.json file
cat << EOF > launch.json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Listen for Xdebug",
            "type": "php",
            "request": "launch",
            "port": 9002,
            "pathMappings": {
                "/application/": "\${workspaceFolder}"
            }
        }
    ]
}
EOF

echo "VSCode configuration setup complete."

