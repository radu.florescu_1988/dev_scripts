#!/bin/bash

SSO_ACCOUNT=$(aws sts get-caller-identity --query "Account" --profile payrix-profile)
#you can add a better check, but this is just an idea for quick check
if [ ${#SSO_ACCOUNT} -eq 14 ];  then 
echo "session still valid" ;
else 
echo "Seems like session expired"

aws sso login --profile payrix-profile

aws ecr get-login-password --profile=payrix-profile --region us-east-1 | docker login --username AWS --password-stdin 812539651325.dkr.ecr.us-east-1.amazonaws.com
# performed login here
fi