#!/bin/bash
clear
echo "Welcome to Payrix Core World!"
echo "This script will be setting up your machine for core api"
~/my_scripts/setup_vscode.sh
(~/my_scripts/aws.sh)

# Default flag for running the xdebug-on command
RUN_XDEBUG=${1:-true}
RUN_SCRIPTS=${2:-true}
RUN_AWS=${3:-false}
RUN_CLEANUP=${4:-false}

# Check if Docker command is available
if ! command -v docker &> /dev/null
then
    echo "Docker could not be found. Please install Docker."
    exit 1
fi

# Check Docker Engine version
echo "Checking Docker Engine version..."
DOCKER_VERSION=$(docker version --format '{{.Server.Version}}' 2> /dev/null)

if [ -z "$DOCKER_VERSION" ]; then
    echo "Failed to retrieve Docker Engine version. Make sure Docker is running."
    exit 1
else
    echo "Docker Engine version: $DOCKER_VERSION"
fi

# Ensuring the default directory exists
if [ ! -d "$HOME/git" ]; then
    mkdir -p "$HOME/git"
fi

# Changing the current directory to the default directory
cd "$HOME/git"

# Existing or additional commands can go here
echo "Current directory set to $(pwd)"

# Ensuring the API directory exists
if [ ! -d "$HOME/git/payrix.auth/api" ]; then
    mkdir -p "$HOME/git/payrix.auth/api"
    echo "API directory created at $HOME/git/payrix.auth/api."
else
    echo "API directory already exists at $HOME/git/payrix.auth/api."
fi

# Check Git configurations
echo "Verifying Git configurations..."
git config --list | grep 'user.name'
git config --list | grep 'user.email'
git config --global core.filemode false

# Ensure the Docker directory exists within the API directory
if [ ! -d "$HOME/git/payrix.auth/api/docker" ]; then
    echo "Docker directory not found within the API folder. Creating now..."
    mkdir -p "$HOME/git/payrix.auth/api/docker"
else
    echo "Docker directory exists within the API folder."
fi

# Check for docker-compose.yml file in the API directory
if [ -f "$HOME/git/payrix.auth/api/docker-compose.yml" ]; then
    echo "docker-compose.yml found. Running Docker Compose..."
    
    if [[ "$RUN_AWS" == "true" ]]; then
        (~/my_scripts/aws.sh)
    fi
    
    if [[ "$RUN_CLEANUP" == "true" ]]; then
        (~/my_scripts/cleanup_docker_containers.sh)
    fi
    (cd "$HOME/git/payrix.auth/api" && docker-compose up -d --build)
    # Execute the initialization script in the Docker container
    if [[ "$RUN_SCRIPTS" == "true" ]]; then
        echo "Running initialization script in the Docker container..."
        (cd "$HOME/git/payrix.auth/api" && docker compose exec -it php-fpm bash -c ./docker/scripts/initialize.sh)
    fi

    # Wait for 20 seconds to allow the script to complete its operations
    echo "Waiting for 5 seconds to ensure the script has completed its operations..."
    sleep 5

    # Check if the xdebug-on command should be run
    if [[ "$RUN_XDEBUG" == "true" ]]; then
        echo "Activating xdebug..."
        (cd "$HOME/git/payrix.auth/api" && docker compose exec -it php-fpm bash -c make xdebug-on)

        echo "Restarting the php-fpm container..."
        (cd "$HOME/git/payrix.auth/api" && docker restart php-fpm)
        echo "php-fpm container has been restarted."
    fi
else
    echo "No docker-compose.yml found in the API folder. Please check the location."
fi

# Existing or additional commands can go here
echo "Current directory set to $(pwd)"