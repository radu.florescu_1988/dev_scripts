#!/bin/bash

# Get the current directory
DIRECTORY=$(pwd)

# Loop over all files in the current directory
for FILE in "$DIRECTORY"/*; do
    # Make each file executable
    chmod +x "$FILE"
    echo "Made executable: $FILE"
done
