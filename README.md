# dev_scripts

here is a list of scripts for your dev machine

# Prerequisites:
- already setup your git repo
- you can clone a repo
- base folder for your code is './git/payrix.api'
- you have all your tools ready

# Tools and versions
- docker 4.28.0
- dbeaver
- vscode

# How to run
- clone repo to your drive into ~/my_scripts/ folder
- run this command ```(chmod +x ~/my_scripts/presetup.sh; ~/my_scripts/presetup.sh)``` this will make all bash runable.
- run ```~/my_scripts/hello.sh```