#!/bin/bash

# Stop all Docker containers
echo "Stopping all Docker containers..."
docker stop $(docker ps -a -q)

# Remove all Docker containers
echo "Removing all Docker containers..."
docker rm $(docker ps -a -q)

echo "All Docker containers have been successfully stopped and removed."

