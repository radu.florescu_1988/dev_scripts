#!/bin/bash

# Default configuration variables
PROJECT_DIR="$HOME/git/payrix.auth/api"    # Default project directory
SERVICE_NAME="php-fpm"        # Default service name

# Retrieve and store the image name of the 'php-fpm' container
IMAGE_NAME=$(docker inspect --format '{{.Config.Image}}' php-fpm)

# Check if the image name was successfully retrieved
if [ -z "$IMAGE_NAME" ]; then
    echo "Failed to retrieve the image name for the 'php-fpm' container."
    exit 1
else
    echo "The image used by 'php-fpm' is '$IMAGE_NAME'."
fi

# Check and assign parameters if provided
if [ ! -z "$1" ]; then
    PROJECT_DIR=$1
fi
if [ ! -z "$2" ]; then
    SERVICE_NAME=$2
fi
if [ ! -z "$3" ]; then
    IMAGE_NAME=$3
fi

# Navigate to the directory containing your docker-compose.yml
cd "$PROJECT_DIR"
(~/my_scripts/aws.sh)
# Stop the container
echo "Stopping the container for service $SERVICE_NAME..."
docker-compose stop "$SERVICE_NAME"

# Remove the container
echo "Removing the container for service $SERVICE_NAME..."
# docker-compose rm -f "$SERVICE_NAME"

# Remove the image
echo "Removing the image $IMAGE_NAME..."
# docker rmi "$IMAGE_NAME"

(cd "$HOME/git/payrix.auth/api" && docker-compose up -d --build)
(cd "$HOME/git/payrix.auth/api" && docker compose exec -it php-fpm bash -c ./docker/scripts/initialize.sh)
(cd "$HOME/git/payrix.auth/api" && docker compose exec -it php-fpm bash -c make xdebug-on)
(cd "$HOME/git/payrix.auth/api" && docker restart php-fpm)

echo "Cleanup completed successfully."
